﻿using Chat.Service.Domain.System.Aggregates;

namespace Chat.Service.Domain.System.Repositories;

public interface IFileSystemRepository : IRepository<FileSystem, Guid>
{
    
}