﻿using Chat.Service.Domain.Users.Aggregates;

namespace Chat.Service.Domain.Users.Repositories;

public interface IFriendRequestRepository : IRepository<FriendRequest>
{
    
}